/*This file is auto generate by pkg.deepin.io/dbus-generator. Don't edit it*/
#include <QtDBus>
QVariant unmarsh(const QVariant&);
QVariant marsh(QDBusArgument target, const QVariant& arg, const QString& sig);
QVariant translateI18n(const char* dir, const char* domain,  const QVariant v);

#ifndef __Drive_H__
#define __Drive_H__

class DriveProxyer: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    DriveProxyer(const QString &path, QObject* parent)
          :QDBusAbstractInterface("pub.geekcentral.Rockstar", path, "org.freedesktop.UDisks2.Drive", QDBusConnection::sessionBus(), parent)
    {
	    if (!isValid()) {
		    qDebug() << "Create Drive remote object failed : " << lastError().message();
	    }
    }
    QVariant fetchProperty(const char* name) {
	QDBusMessage msg = QDBusMessage::createMethodCall(service(), path(),
		QLatin1String("org.freedesktop.DBus.Properties"),
		QLatin1String("Get"));
	msg << interface() << QString::fromUtf8(name);
	QDBusMessage reply = connection().call(msg, QDBus::Block, timeout());
	if (reply.type() != QDBusMessage::ReplyMessage) {
	    qDebug () << QDBusError(reply) << "at " << service() << path() << interface() << name;
	    return QVariant();
	}
	if (reply.signature() != QLatin1String("v")) {
	    QString errmsg = QLatin1String("Invalid signature org.freedesktop.DBus.Propertyies in return from call to ");
	    qDebug () << QDBusError(QDBusError::InvalidSignature, errmsg.arg(reply.signature()));
	    return QVariant();
	}

	QVariant value = qvariant_cast<QDBusVariant>(reply.arguments().at(0)).variant();
	return value;
    }


    Q_PROPERTY(QDBusVariant Vendor READ __get_Vendor__ )
    QDBusVariant __get_Vendor__() { return QDBusVariant(fetchProperty("Vendor")); }
    

    Q_PROPERTY(QDBusVariant Model READ __get_Model__ )
    QDBusVariant __get_Model__() { return QDBusVariant(fetchProperty("Model")); }
    

    Q_PROPERTY(QDBusVariant Revision READ __get_Revision__ )
    QDBusVariant __get_Revision__() { return QDBusVariant(fetchProperty("Revision")); }
    

    Q_PROPERTY(QDBusVariant Serial READ __get_Serial__ )
    QDBusVariant __get_Serial__() { return QDBusVariant(fetchProperty("Serial")); }
    

    Q_PROPERTY(QDBusVariant WWN READ __get_WWN__ )
    QDBusVariant __get_WWN__() { return QDBusVariant(fetchProperty("WWN")); }
    

    Q_PROPERTY(QDBusVariant Id READ __get_Id__ )
    QDBusVariant __get_Id__() { return QDBusVariant(fetchProperty("Id")); }
    

    Q_PROPERTY(QDBusVariant Configuration READ __get_Configuration__ )
    QDBusVariant __get_Configuration__() { return QDBusVariant(fetchProperty("Configuration")); }
    

    Q_PROPERTY(QDBusVariant Media READ __get_Media__ )
    QDBusVariant __get_Media__() { return QDBusVariant(fetchProperty("Media")); }
    

    Q_PROPERTY(QDBusVariant MediaCompatibility READ __get_MediaCompatibility__ )
    QDBusVariant __get_MediaCompatibility__() { return QDBusVariant(fetchProperty("MediaCompatibility")); }
    

    Q_PROPERTY(QDBusVariant MediaRemovable READ __get_MediaRemovable__ )
    QDBusVariant __get_MediaRemovable__() { return QDBusVariant(fetchProperty("MediaRemovable")); }
    

    Q_PROPERTY(QDBusVariant MediaAvailable READ __get_MediaAvailable__ )
    QDBusVariant __get_MediaAvailable__() { return QDBusVariant(fetchProperty("MediaAvailable")); }
    

    Q_PROPERTY(QDBusVariant MediaChangeDetected READ __get_MediaChangeDetected__ )
    QDBusVariant __get_MediaChangeDetected__() { return QDBusVariant(fetchProperty("MediaChangeDetected")); }
    

    Q_PROPERTY(QDBusVariant Size READ __get_Size__ )
    QDBusVariant __get_Size__() { return QDBusVariant(fetchProperty("Size")); }
    

    Q_PROPERTY(QDBusVariant TimeDetected READ __get_TimeDetected__ )
    QDBusVariant __get_TimeDetected__() { return QDBusVariant(fetchProperty("TimeDetected")); }
    

    Q_PROPERTY(QDBusVariant TimeMediaDetected READ __get_TimeMediaDetected__ )
    QDBusVariant __get_TimeMediaDetected__() { return QDBusVariant(fetchProperty("TimeMediaDetected")); }
    

    Q_PROPERTY(QDBusVariant Optical READ __get_Optical__ )
    QDBusVariant __get_Optical__() { return QDBusVariant(fetchProperty("Optical")); }
    

    Q_PROPERTY(QDBusVariant OpticalBlank READ __get_OpticalBlank__ )
    QDBusVariant __get_OpticalBlank__() { return QDBusVariant(fetchProperty("OpticalBlank")); }
    

    Q_PROPERTY(QDBusVariant OpticalNumTracks READ __get_OpticalNumTracks__ )
    QDBusVariant __get_OpticalNumTracks__() { return QDBusVariant(fetchProperty("OpticalNumTracks")); }
    

    Q_PROPERTY(QDBusVariant OpticalNumAudioTracks READ __get_OpticalNumAudioTracks__ )
    QDBusVariant __get_OpticalNumAudioTracks__() { return QDBusVariant(fetchProperty("OpticalNumAudioTracks")); }
    

    Q_PROPERTY(QDBusVariant OpticalNumDataTracks READ __get_OpticalNumDataTracks__ )
    QDBusVariant __get_OpticalNumDataTracks__() { return QDBusVariant(fetchProperty("OpticalNumDataTracks")); }
    

    Q_PROPERTY(QDBusVariant OpticalNumSessions READ __get_OpticalNumSessions__ )
    QDBusVariant __get_OpticalNumSessions__() { return QDBusVariant(fetchProperty("OpticalNumSessions")); }
    

    Q_PROPERTY(QDBusVariant RotationRate READ __get_RotationRate__ )
    QDBusVariant __get_RotationRate__() { return QDBusVariant(fetchProperty("RotationRate")); }
    

    Q_PROPERTY(QDBusVariant ConnectionBus READ __get_ConnectionBus__ )
    QDBusVariant __get_ConnectionBus__() { return QDBusVariant(fetchProperty("ConnectionBus")); }
    

    Q_PROPERTY(QDBusVariant Seat READ __get_Seat__ )
    QDBusVariant __get_Seat__() { return QDBusVariant(fetchProperty("Seat")); }
    

    Q_PROPERTY(QDBusVariant Removable READ __get_Removable__ )
    QDBusVariant __get_Removable__() { return QDBusVariant(fetchProperty("Removable")); }
    

    Q_PROPERTY(QDBusVariant Ejectable READ __get_Ejectable__ )
    QDBusVariant __get_Ejectable__() { return QDBusVariant(fetchProperty("Ejectable")); }
    

    Q_PROPERTY(QDBusVariant SortKey READ __get_SortKey__ )
    QDBusVariant __get_SortKey__() { return QDBusVariant(fetchProperty("SortKey")); }
    

    Q_PROPERTY(QDBusVariant CanPowerOff READ __get_CanPowerOff__ )
    QDBusVariant __get_CanPowerOff__() { return QDBusVariant(fetchProperty("CanPowerOff")); }
    

    Q_PROPERTY(QDBusVariant SiblingId READ __get_SiblingId__ )
    QDBusVariant __get_SiblingId__() { return QDBusVariant(fetchProperty("SiblingId")); }
    


Q_SIGNALS:
};

class Drive : public QObject 
{
    Q_OBJECT
private:
    QString m_path;
    Q_SLOT void _propertiesChanged(const QDBusMessage &msg) {
	    QList<QVariant> arguments = msg.arguments();
	    if (3 != arguments.count())
	    	return;
	    QString interfaceName = msg.arguments().at(0).toString();
	    if (interfaceName != "org.freedesktop.UDisks2.Drive")
		    return;

	    QVariantMap changedProps = qdbus_cast<QVariantMap>(arguments.at(1).value<QDBusArgument>());
	    foreach(const QString &prop, changedProps.keys()) {
		    if (0) { 
		    } else if (prop == "Vendor") {
			    Q_EMIT __vendorChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Model") {
			    Q_EMIT __modelChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Revision") {
			    Q_EMIT __revisionChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Serial") {
			    Q_EMIT __serialChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "WWN") {
			    Q_EMIT __wWNChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Id") {
			    Q_EMIT __idChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Configuration") {
			    Q_EMIT __configurationChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Media") {
			    Q_EMIT __mediaChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "MediaCompatibility") {
			    Q_EMIT __mediaCompatibilityChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "MediaRemovable") {
			    Q_EMIT __mediaRemovableChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "MediaAvailable") {
			    Q_EMIT __mediaAvailableChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "MediaChangeDetected") {
			    Q_EMIT __mediaChangeDetectedChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Size") {
			    Q_EMIT __sizeChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "TimeDetected") {
			    Q_EMIT __timeDetectedChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "TimeMediaDetected") {
			    Q_EMIT __timeMediaDetectedChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Optical") {
			    Q_EMIT __opticalChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "OpticalBlank") {
			    Q_EMIT __opticalBlankChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "OpticalNumTracks") {
			    Q_EMIT __opticalNumTracksChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "OpticalNumAudioTracks") {
			    Q_EMIT __opticalNumAudioTracksChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "OpticalNumDataTracks") {
			    Q_EMIT __opticalNumDataTracksChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "OpticalNumSessions") {
			    Q_EMIT __opticalNumSessionsChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "RotationRate") {
			    Q_EMIT __rotationRateChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "ConnectionBus") {
			    Q_EMIT __connectionBusChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Seat") {
			    Q_EMIT __seatChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Removable") {
			    Q_EMIT __removableChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Ejectable") {
			    Q_EMIT __ejectableChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "SortKey") {
			    Q_EMIT __sortKeyChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "CanPowerOff") {
			    Q_EMIT __canPowerOffChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "SiblingId") {
			    Q_EMIT __siblingIdChanged__(unmarsh(changedProps.value(prop)));
		    }
	    }
    }
    void _rebuild() 
    { 
	  delete m_ifc;
          m_ifc = new DriveProxyer(m_path, this);
	  _setupSignalHandle();
    }
    void _setupSignalHandle() {

    }
public:
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    const QString path() {
	    return m_path;
    }
    void setPath(const QString& path) {
	    QDBusConnection::sessionBus().disconnect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				 this, SLOT(_propertiesChanged(QDBusMessage)));
	    m_path = path;
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
	    _rebuild();
    }
    Q_SIGNAL void pathChanged(QString);

    Drive(QObject *parent=0) : QObject(parent), m_ifc(new DriveProxyer("/org/freedesktop/UDisks2/Drive", this))
    {
	    _setupSignalHandle();
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
    }
    
    Q_PROPERTY(QVariant vendor READ __get_Vendor__  NOTIFY __vendorChanged__)
    Q_PROPERTY(QVariant model READ __get_Model__  NOTIFY __modelChanged__)
    Q_PROPERTY(QVariant revision READ __get_Revision__  NOTIFY __revisionChanged__)
    Q_PROPERTY(QVariant serial READ __get_Serial__  NOTIFY __serialChanged__)
    Q_PROPERTY(QVariant wWN READ __get_WWN__  NOTIFY __wWNChanged__)
    Q_PROPERTY(QVariant id READ __get_Id__  NOTIFY __idChanged__)
    Q_PROPERTY(QVariant configuration READ __get_Configuration__  NOTIFY __configurationChanged__)
    Q_PROPERTY(QVariant media READ __get_Media__  NOTIFY __mediaChanged__)
    Q_PROPERTY(QVariant mediaCompatibility READ __get_MediaCompatibility__  NOTIFY __mediaCompatibilityChanged__)
    Q_PROPERTY(QVariant mediaRemovable READ __get_MediaRemovable__  NOTIFY __mediaRemovableChanged__)
    Q_PROPERTY(QVariant mediaAvailable READ __get_MediaAvailable__  NOTIFY __mediaAvailableChanged__)
    Q_PROPERTY(QVariant mediaChangeDetected READ __get_MediaChangeDetected__  NOTIFY __mediaChangeDetectedChanged__)
    Q_PROPERTY(QVariant size READ __get_Size__  NOTIFY __sizeChanged__)
    Q_PROPERTY(QVariant timeDetected READ __get_TimeDetected__  NOTIFY __timeDetectedChanged__)
    Q_PROPERTY(QVariant timeMediaDetected READ __get_TimeMediaDetected__  NOTIFY __timeMediaDetectedChanged__)
    Q_PROPERTY(QVariant optical READ __get_Optical__  NOTIFY __opticalChanged__)
    Q_PROPERTY(QVariant opticalBlank READ __get_OpticalBlank__  NOTIFY __opticalBlankChanged__)
    Q_PROPERTY(QVariant opticalNumTracks READ __get_OpticalNumTracks__  NOTIFY __opticalNumTracksChanged__)
    Q_PROPERTY(QVariant opticalNumAudioTracks READ __get_OpticalNumAudioTracks__  NOTIFY __opticalNumAudioTracksChanged__)
    Q_PROPERTY(QVariant opticalNumDataTracks READ __get_OpticalNumDataTracks__  NOTIFY __opticalNumDataTracksChanged__)
    Q_PROPERTY(QVariant opticalNumSessions READ __get_OpticalNumSessions__  NOTIFY __opticalNumSessionsChanged__)
    Q_PROPERTY(QVariant rotationRate READ __get_RotationRate__  NOTIFY __rotationRateChanged__)
    Q_PROPERTY(QVariant connectionBus READ __get_ConnectionBus__  NOTIFY __connectionBusChanged__)
    Q_PROPERTY(QVariant seat READ __get_Seat__  NOTIFY __seatChanged__)
    Q_PROPERTY(QVariant removable READ __get_Removable__  NOTIFY __removableChanged__)
    Q_PROPERTY(QVariant ejectable READ __get_Ejectable__  NOTIFY __ejectableChanged__)
    Q_PROPERTY(QVariant sortKey READ __get_SortKey__  NOTIFY __sortKeyChanged__)
    Q_PROPERTY(QVariant canPowerOff READ __get_CanPowerOff__  NOTIFY __canPowerOffChanged__)
    Q_PROPERTY(QVariant siblingId READ __get_SiblingId__  NOTIFY __siblingIdChanged__)

    //Property read methods
    const QVariant __get_Vendor__() { return unmarsh(m_ifc->__get_Vendor__().variant()); }
    const QVariant __get_Model__() { return unmarsh(m_ifc->__get_Model__().variant()); }
    const QVariant __get_Revision__() { return unmarsh(m_ifc->__get_Revision__().variant()); }
    const QVariant __get_Serial__() { return unmarsh(m_ifc->__get_Serial__().variant()); }
    const QVariant __get_WWN__() { return unmarsh(m_ifc->__get_WWN__().variant()); }
    const QVariant __get_Id__() { return unmarsh(m_ifc->__get_Id__().variant()); }
    const QVariant __get_Configuration__() { return unmarsh(m_ifc->__get_Configuration__().variant()); }
    const QVariant __get_Media__() { return unmarsh(m_ifc->__get_Media__().variant()); }
    const QVariant __get_MediaCompatibility__() { return unmarsh(m_ifc->__get_MediaCompatibility__().variant()); }
    const QVariant __get_MediaRemovable__() { return unmarsh(m_ifc->__get_MediaRemovable__().variant()); }
    const QVariant __get_MediaAvailable__() { return unmarsh(m_ifc->__get_MediaAvailable__().variant()); }
    const QVariant __get_MediaChangeDetected__() { return unmarsh(m_ifc->__get_MediaChangeDetected__().variant()); }
    const QVariant __get_Size__() { return unmarsh(m_ifc->__get_Size__().variant()); }
    const QVariant __get_TimeDetected__() { return unmarsh(m_ifc->__get_TimeDetected__().variant()); }
    const QVariant __get_TimeMediaDetected__() { return unmarsh(m_ifc->__get_TimeMediaDetected__().variant()); }
    const QVariant __get_Optical__() { return unmarsh(m_ifc->__get_Optical__().variant()); }
    const QVariant __get_OpticalBlank__() { return unmarsh(m_ifc->__get_OpticalBlank__().variant()); }
    const QVariant __get_OpticalNumTracks__() { return unmarsh(m_ifc->__get_OpticalNumTracks__().variant()); }
    const QVariant __get_OpticalNumAudioTracks__() { return unmarsh(m_ifc->__get_OpticalNumAudioTracks__().variant()); }
    const QVariant __get_OpticalNumDataTracks__() { return unmarsh(m_ifc->__get_OpticalNumDataTracks__().variant()); }
    const QVariant __get_OpticalNumSessions__() { return unmarsh(m_ifc->__get_OpticalNumSessions__().variant()); }
    const QVariant __get_RotationRate__() { return unmarsh(m_ifc->__get_RotationRate__().variant()); }
    const QVariant __get_ConnectionBus__() { return unmarsh(m_ifc->__get_ConnectionBus__().variant()); }
    const QVariant __get_Seat__() { return unmarsh(m_ifc->__get_Seat__().variant()); }
    const QVariant __get_Removable__() { return unmarsh(m_ifc->__get_Removable__().variant()); }
    const QVariant __get_Ejectable__() { return unmarsh(m_ifc->__get_Ejectable__().variant()); }
    const QVariant __get_SortKey__() { return unmarsh(m_ifc->__get_SortKey__().variant()); }
    const QVariant __get_CanPowerOff__() { return unmarsh(m_ifc->__get_CanPowerOff__().variant()); }
    const QVariant __get_SiblingId__() { return unmarsh(m_ifc->__get_SiblingId__().variant()); }
    //Property set methods :TODO check access

public Q_SLOTS:  
    QVariant Eject(const QVariant &options) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), options, "a{sv}");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("Eject"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UDisks2.Drive.Eject:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant SetConfiguration(const QVariant &value, const QVariant &options) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), value, "a{sv}") << marsh(QDBusArgument(), options, "a{sv}");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("SetConfiguration"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UDisks2.Drive.SetConfiguration:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant PowerOff(const QVariant &options) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), options, "a{sv}");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("PowerOff"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UDisks2.Drive.PowerOff:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }


Q_SIGNALS:
//Property changed notify signal
    void __vendorChanged__(QVariant);
    void __modelChanged__(QVariant);
    void __revisionChanged__(QVariant);
    void __serialChanged__(QVariant);
    void __wWNChanged__(QVariant);
    void __idChanged__(QVariant);
    void __configurationChanged__(QVariant);
    void __mediaChanged__(QVariant);
    void __mediaCompatibilityChanged__(QVariant);
    void __mediaRemovableChanged__(QVariant);
    void __mediaAvailableChanged__(QVariant);
    void __mediaChangeDetectedChanged__(QVariant);
    void __sizeChanged__(QVariant);
    void __timeDetectedChanged__(QVariant);
    void __timeMediaDetectedChanged__(QVariant);
    void __opticalChanged__(QVariant);
    void __opticalBlankChanged__(QVariant);
    void __opticalNumTracksChanged__(QVariant);
    void __opticalNumAudioTracksChanged__(QVariant);
    void __opticalNumDataTracksChanged__(QVariant);
    void __opticalNumSessionsChanged__(QVariant);
    void __rotationRateChanged__(QVariant);
    void __connectionBusChanged__(QVariant);
    void __seatChanged__(QVariant);
    void __removableChanged__(QVariant);
    void __ejectableChanged__(QVariant);
    void __sortKeyChanged__(QVariant);
    void __canPowerOffChanged__(QVariant);
    void __siblingIdChanged__(QVariant);

//DBus Interface's signal
private:
    DriveProxyer *m_ifc;
};

#endif
