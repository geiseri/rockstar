#ifndef WEATHER_H
#define WEATHER_H

#include <QObject>
#include "weather_adaptor.h"

class QNetworkReply;
class QNetworkAccessManager;

class Weather : public QObject
{
    Q_OBJECT
public:
    explicit Weather(QObject *parent = 0);

    Q_PROPERTY(double humidity READ humidity)
    double humidity() const;

    Q_PROPERTY(QString iconSource READ iconSource)
    QString iconSource() const;

    Q_PROPERTY(int location READ location WRITE setLocation )
    int location() const;
    void setLocation( int loc );

    Q_PROPERTY(QString name READ name)
    QString name() const;

    Q_PROPERTY(double pressure READ pressure)
    double pressure() const;

    Q_PROPERTY(double temp READ temp)
    double temp() const;

    Q_PROPERTY(QString text READ text)
    QString text() const;

    Q_PROPERTY(QString windDirection READ windDirection)
    QString windDirection() const;

    Q_PROPERTY(double windSpeed READ windSpeed)
    double windSpeed() const;

    Q_PROPERTY(bool metric READ metric WRITE setMetric )
    bool metric() const;
    void setMetric(bool metric);


    Q_PROPERTY(QString zipcode READ zipcode WRITE setZipcode )
    QString zipcode() const;
    void setZipcode(const QString &zipcode);

private slots:
    void refreshWeather();
    void queryFinished( QNetworkReply *reply);

private:
    void emitPropertyChanged(const QString &name,
                              const QVariant &value);
private:
    WeatherAdaptor *m_adaptor;
    double m_humidity;
    QString m_iconSource;
    int m_location;
    QString m_name;
    double m_pressure;
    double m_temp;
    QString m_text;
    QString m_windDirection;
    double m_windSpeed;
    bool m_metric;
    QString m_zipcode;
    QNetworkAccessManager *m_nam;

};



#endif // WEATHER_H
