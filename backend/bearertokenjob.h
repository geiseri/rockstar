#ifndef BEARERTOKENJOB_H
#define BEARERTOKENJOB_H

#include <QObject>
#include <QDateTime>

class QNetworkAccessManager;

struct AccessToken {
    QString token;
    QDateTime expire;
};

class BearerTokenJob : public QObject
{
    Q_OBJECT
public:
    explicit BearerTokenJob(QNetworkAccessManager *parent = 0);

    void getToken(const QString &clientID, const QString &clientSecret, const AccessToken &token , const QString &refreshToken);

signals:
    void accessToken( const AccessToken &token );
    void finished();

private slots:
    void extractToken();

private:
    QNetworkAccessManager *m_nam;
};

#endif // BEARERTOKENJOB_H
