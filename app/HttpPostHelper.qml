import QtQuick 2.0

import "UrlUtilities.js" as UrlUtilities
import "ObjectUtilities.js" as ObjectUtilities

QtObject {
    id: postHelper

    property var json: null
    property string url: ""
    property int status: 0
    property var headers: new Object


    function post(content,requestHeaders) {
        var request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if (request.readyState === XMLHttpRequest.DONE) {
                postHelper.headers = request.getAllResponseHeaders();
                postHelper.status = request.status;

                console.log(request.responseText);
                var jsonResponse = JSON.parse(request.responseText);
                if (jsonResponse.errors !== undefined) {
                    jsonResponse.errors.forEach(function(err) {
                        console.log("JSON error: " + err.message)
                    });
                } else {
                    postHelper.json = jsonResponse;
                }
            }
        }

        request.open("POST", url);

        Object.getOwnPropertyNames(requestHeaders).forEach(function(val) {
            request.setRequestHeader(val,requestHeaders[val]);
        });

        request.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
        request.send(UrlUtilities.makeQuery( content ));
    }
}

