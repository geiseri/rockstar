import QtQuick 2.4
import QtQuick.Layouts 1.1
import Material 0.3
import Material.ListItems 0.1

BaseListItem {
    id: listItem

    property alias text: label.text
    property alias iconSource: icon.source
    property alias iconName: icon.name

    height: dp(48)
    width: dp(256)

    RowLayout {
        anchors.fill: parent

        anchors.leftMargin: listItem.margins
        anchors.rightMargin: listItem.margins

        spacing: dp(5)

        Item {
            Layout.preferredWidth: dp(40)
            Layout.preferredHeight: width
            Layout.alignment: Qt.AlignCenter

            Icon {
                id: icon

                anchors {
                    verticalCenter: parent.verticalCenter
                    left: parent.left
                }
                size: dp(24)
            }
        }

        Label {
            id: label

            Layout.alignment: Qt.AlignVCenter
            Layout.fillWidth: true

            elide: Text.ElideRight
            style: "subheading"
        }


    }
}
